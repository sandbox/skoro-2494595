<?php

/**
 * @file
 * Provides simple crop image effect.
 */

/**
 * Implements hook_image_effect_info().
 */
function simple_crop_image_effect_info() {
  return array(
    'simple_crop' => array(
      'label' => t('Simple crop'),
      'help' => t('Cropping will remove portions of an image to make it the specified dimensions.'),
      'effect callback' => 'simple_crop_image_crop_effect',
      'dimensions callback' => 'image_resize_dimensions',
      'form callback' => 'image_resize_form',
      'summary theme' => 'image_crop_summary',
    ),
  );
}

/**
 * Image effect callback: simple crop.
 */
function simple_crop_image_crop_effect(&$image, $data) {

  if (image_get_toolkit() != 'gd') {
    watchdog('simple_crop', 'Only GD toolkit supported.');
    return FALSE;
  }
  
  $dst_x = $dst_y = $src_x = $src_y = 0;
  $dst_w = $data['width'];
  $dst_h = $data['height'];
  $src_w = $image->info['width'];
  $src_h = $image->info['height'];
  
  // Calculate horizontal anchor.
  if ($dst_w > $src_w) {
    $dst_x = ($dst_w - $src_w) / 2;
  }
  elseif ($dst_w < $src_w) {
    $src_x = ($src_w - $dst_w) / 2;
  }
  
  // Calculate vertical anchor.
  if ($dst_h > $src_h) {
    $dst_y = ($dst_h - $src_h) / 2;
  }
  elseif ($dst_h < $src_h) {
    $src_y = ($src_h - $dst_h) / 2;
  }
  
  // Prepare destination blank image.
  $blank = image_gd_create_tmp($image, $dst_w, $dst_h);
  
  if (!imagecopy($blank, $image->resource, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h)) {
    return FALSE;
  }
  
  // Destroy the original image and return the modified image.
  imagedestroy($image->resource);
  
  $image->resource = $blank;
  $image->info['width'] = $dst_w;
  $image->info['height'] = $dst_h;
  
  return TRUE;
}
